import {
  Controller,
  Post,
  Get,
  Body,
  Put,
  Delete,
  ParseIntPipe,
  Param,
} from '@nestjs/common';
import {
  CreateUserDto,
  PostDetailsDto,
  ProfileDetailsDto,
  UpdatedUserDto,
} from '../../dtos/user.dto';
import { UsersService } from '../../services/users/users.service';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}
  @Get()
  getUsers() {
    return this.userService.fetchUsers();
  }

  @Post()
  createUser(@Body() createUserDto: CreateUserDto) {
    return this.userService.createUser(createUserDto);
  }
  @Put(':id')
  async updateUserById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updatedUser: UpdatedUserDto,
  ) {
    await this.userService.updateUserById(id, updatedUser);
  }
  @Delete(':id')
  async deleteUser(@Param('id', ParseIntPipe) id: number) {
    await this.userService.deleteUser(id);
  }
  @Post(':id/profile')
  createProfile(
    @Param('id', ParseIntPipe) id: number,
    @Body() profileDetails: ProfileDetailsDto,
  ) {
    return this.userService.createProfile(id, profileDetails);
  }
  @Post(':id/post')
  createPost(
    @Param('id', ParseIntPipe) id: number,
    @Body() postDetails: PostDetailsDto,
  ) {
    return this.userService.createPost(id, postDetails);
  }
}
