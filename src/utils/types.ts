export type CreateUserParams = {
  username: string;
  password: string;
};
export type CreateProfileParams = {
  firstname: string;
  lastname: string;
  age: number;
  dateOfBirth: string;
};
export type CreatePostParams = {
  title: string;
  description: string;
  releaseDate: string;
};
