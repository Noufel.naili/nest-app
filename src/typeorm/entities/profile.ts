import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
@Entity({ name: 'profiles' })
export class Profile {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  firstname: string;
  @Column()
  lastname: string;
  @Column()
  pictureLink: string;
  @Column()
  age: number;
  @Column()
  dateOfBirth: string;
}
